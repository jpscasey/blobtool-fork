install java 8 (or a docker container?)

set the following environment variables:

DB_URL				=  10.27.81.107
DB_NAME_SLT		=  NS_Dev_Spectrum
DB_NAME_VIRM	=  NS_Dev_Spectrum_VIRM
DB_NAME_SNG	=  NS_Dev_Spectrum_SNG
DB_INSTANCE		= {not required for QA env}
DB_USER				= spectrumAppUserDev
DB_PWD				= N1e2x345$


run the extract tool using the bat file

it accepts the following parameters:

FLEET 					=		SLT/SNG/VIRM (only used for SNG currently)
START_TIME 		=		yyyy-mm-dd format only
END_TIME    		=		yyyy-mm-dd format only
UNIT  					=		all, or unitnumber string e.g. 2308 (single unit only)
CHANNELS			=		all, or channelname string comma-spearated e.g. "ETH_A1_DOOR1_A1_DR_CYCLE_COUNTER,ETH_A1_DOOR2_B_SS_CYCLE_COUNTER,AL2111_A2,AL2197_A2,..."

e.g.
run.bat SNG 2018-11-01 2018-12-01 2310 all

run.bat SNG 2018-11-01 2018-12-01 all "ETH_A1_DOOR1_A1_DR_CYCLE_COUNTER,ETH_A1_DOOR2_B_SS_CYCLE_COUNTER,AL2111_A2,AL2197_A2,ETH_A1_DOOR4_A2_DR_CYCLE_COUNTER,ETH_A1_DOOR2_A2_DR_CYCLE_COUNTER,AL1449_A2,AL1450_A1"