package com.trimble.r2m.blobtool.file;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import com.trimble.r2m.blobtool.bean.ChannelDataBT;
import com.trimble.r2m.blobtool.bean.ChannelValueBT;

public class FileWriterCSV {

    public void createCSVFile(List<ChannelDataBT> channelData) throws IOException {
        System.out.println("Using system directory: " + System.getProperty("user.dir"));
                
        FileWriter writer = new FileWriter(System.getProperty("user.dir") + "\\blobexport-" + String.valueOf(Instant.now().getEpochSecond()) + ".csv");
        
        List<String> headers = new ArrayList<String>();
        headers.add("UnitNumber");
        headers.add("TimeStamp");
        for(ChannelValueBT channel : channelData.get(0).getData()) {
            headers.add(channel.getChannelName());
        }
        String[] headerArray = new String[headers.size()];
        headerArray = headers.toArray(headerArray);
        
        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(headerArray));
        
        for(ChannelDataBT data : channelData) {
            List<Object> valueList = new ArrayList<Object>();
            valueList.add(data.getUnitNumber());
            valueList.add(data.getTimestamp());
            for(ChannelValueBT value : data.getData()) {
                valueList.add(value.getChannelValue());
            }
            csvPrinter.printRecord(valueList.toArray());
        }
        
        csvPrinter.flush();
        csvPrinter.close();
    }
}
