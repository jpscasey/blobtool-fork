package com.trimble.r2m.blobtool.provider;

import org.apache.commons.dbcp2.BasicDataSource;

import com.trimble.r2m.blobtool.config.Configuration;
import com.typesafe.config.Config;

public class DataSourceProvider {
    
    public BasicDataSource getDataSource(String fleetId) {
        Config dbConfs = Configuration.get().getConfig("dbConfig");
                
        BasicDataSource bdsx = new BasicDataSource();
        Config fleetDbConf = dbConfs.getConfig(fleetId);
        
        
        bdsx.setInitialSize(10);
        bdsx.setDriverClassName(fleetDbConf.getString("driverClassName"));
        bdsx.setUrl(fleetDbConf.getString("url"));
        bdsx.setUsername(fleetDbConf.getString("user"));
        bdsx.setPassword(fleetDbConf.getString("password"));
        bdsx.setValidationQuery("select 1");
        bdsx.setTimeBetweenEvictionRunsMillis(30000);
        bdsx.setTestWhileIdle(true);
        bdsx.setTestOnBorrow(false);
        bdsx.setTestOnReturn(false);

        return bdsx;
    }

}
