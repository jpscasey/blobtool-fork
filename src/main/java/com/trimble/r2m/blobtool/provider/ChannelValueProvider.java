package com.trimble.r2m.blobtool.provider;

import java.util.List;

import javax.sql.DataSource;

import com.trimble.r2m.blobtool.bean.ChannelDataBT;
import com.trimble.r2m.blobtool.caching.ChannelCache;
import com.trimble.r2m.blobtool.persistence.ChannelValueDao;

public class ChannelValueProvider {
    
    private final ChannelValueDao channelValueDao;
    
    public ChannelValueProvider(DataSource ds, ChannelCache cache) {
        this.channelValueDao = new ChannelValueDao(ds, cache);
    }
    
    public List<ChannelDataBT> getChannelData(String from, String to, String channels) {
        if (channels != null && channels.equals("all")) {
            List<ChannelDataBT> channelData = channelValueDao.getData(from, to);
            return channelData;
        } else {
            List<ChannelDataBT> channelData = channelValueDao.getDataSubset(from, to, channels);
            return channelData;
        }
    }
    
    public List<ChannelDataBT> getChannelDataUnit(String from, String to, String channels, String unit) {
        if (channels != null && channels.equals("all")) {
            List<ChannelDataBT> channelData = channelValueDao.getUnitData(from, to, unit);
            return channelData;
        } else {
            List<ChannelDataBT> channelData = channelValueDao.getUnitDataSubset(from, to, channels, unit);
            return channelData;
        }
    }

}
