package com.trimble.r2m.blobtool.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;

import com.trimble.r2m.blobtool.bean.ChannelConfigBT;
import com.trimble.r2m.blobtool.bean.ChannelSchemaBT;
import com.trimble.r2m.blobtool.config.Configuration;

public class ChannelDao extends NamedParameterJdbcDaoSupport {
    
    public ChannelDao(DataSource ds) {
        this.setDataSource(ds);
    }
    
    /**
     * Returns a map of ChannelName-ChannelConfig pairs
     * @return
     */
    public Map<String, ChannelConfigBT> getChannelConfig() {
        Map<String, ChannelConfigBT> result = new HashMap<String, ChannelConfigBT>();
        
        String query = Configuration.getQuery("getChannelConfigList");
        
        List<ChannelConfigBT> list = this.getJdbcTemplate().query(
                query,
                new RowMapper<ChannelConfigBT>() {
                    public ChannelConfigBT mapRow(ResultSet rs, int rowNum) throws SQLException {
                        ChannelConfigBT channelConf = new ChannelConfigBT();
                        channelConf.setName(rs.getString("Name"));
                        channelConf.setId(rs.getInt("ID"));
                        channelConf.setDataType(rs.getString("DataType"));
                        channelConf.setMinValue(rs.getInt("MinValue"));
                        channelConf.setMaxValue(rs.getDouble("MaxValue"));
                        channelConf.setHardwareType(rs.getString("HardwareType"));
                        channelConf.setColNum(rs.getInt("EngColumnNo"));
                        return channelConf;
                    }
        });
        
        for (ChannelConfigBT conf : list) {
            result.put(conf.getName(), conf);
        }
        
        return result;
    }
    
    /**
     * Returns a list of schema_version-ChannelSchema pairs
     * @return
     */
    public Map<Integer, ChannelSchemaBT> getChannelSchema() {
        Map<Integer, ChannelSchemaBT> result = new HashMap<Integer, ChannelSchemaBT>();
        
        String query = Configuration.getQuery("getChannelSchema");
        List<ChannelSchemaBT> schemaList = new ArrayList<ChannelSchemaBT>();

        schemaList = this.getJdbcTemplate().query(
                query, 
                new RowMapper<ChannelSchemaBT>() {
                        public ChannelSchemaBT mapRow(ResultSet rs, int rowNum) throws SQLException {
                            ChannelSchemaBT channelSchema = new ChannelSchemaBT();
                            channelSchema.setSchemaId(rs.getInt("ID"));
                            channelSchema.setSchemaString(rs.getString("ChannelSchema"));
                            return channelSchema;
                }
                
        });
        
        for (ChannelSchemaBT schema : schemaList) {
            result.put(schema.getSchemaId(), schema);
        }

        return result;
    }

}
