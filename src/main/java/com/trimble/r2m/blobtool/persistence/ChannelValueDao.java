package com.trimble.r2m.blobtool.persistence;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.trimble.r2m.blobtool.bean.ChannelDataBT;
import com.trimble.r2m.blobtool.bean.ChannelDataBTSubset;
import com.trimble.r2m.blobtool.bean.ChannelValueBT;
import com.trimble.r2m.blobtool.caching.ChannelCache;
import com.trimble.r2m.blobtool.config.Configuration;
import com.trimble.r2m.combine.CompactData;
import com.trimble.r2m.combine.CompactQuery;
import com.trimble.r2m.combine.CompactSchema;
import com.trimble.r2m.combine.SqlDialect;

public class ChannelValueDao extends NamedParameterJdbcDaoSupport {
    
    ChannelCache cache;
        
    public ChannelValueDao(DataSource ds, ChannelCache channelCache) {
        this.setDataSource(ds);
        this.cache = channelCache;
    }
    
    /**
     * get data for all units between a range of timestamps
     * @param from
     * @param to
     * @param unitNumber
     * @return
     */
    public List<ChannelDataBT> getData(String from, String to) {
        List<ChannelDataBT> result = new ArrayList<ChannelDataBT>();
        
        String query = Configuration.getQuery("getChannelData");
        
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("fromDate", LocalDate.parse(from))
                .addValue("toDate", LocalDate.parse(to));
        
        result = this.getNamedParameterJdbcTemplate().query(query, params, new ChannelValueBTRowMapper());
        
        return result;
    }
    
    /**
     * get data for a single unit between a range of timestamps
     * @param from
     * @param to
     * @param unitNumber
     * @return
     */
    public List<ChannelDataBT> getUnitData(String from, String to, String unitNumber) {
        List<ChannelDataBT> result = new ArrayList<ChannelDataBT>();
        
        String query = Configuration.getQuery("getChannelDataByUnit");
        
        SqlParameterSource params = new MapSqlParameterSource()
                                        .addValue("fromDate", LocalDate.parse(from))
                                        .addValue("toDate", LocalDate.parse(to))
                                        .addValue("unitNumber", unitNumber);
        
        result = this.getNamedParameterJdbcTemplate().query(query, params, new ChannelValueBTRowMapper());
        
        return result;
    }
    
    /**
     * get data for all units returning only a subset of channels (uses r2m-combine CompactQuery)
     * @param from
     * @param to
     * @param channels
     * @param unitNumber
     * @return
     */
    public List<ChannelDataBT> getDataSubset(String from, String to, String channels) {
        List<ChannelDataBT> result = new ArrayList<ChannelDataBT>();
        
        // build input params into CompactQuery-friendly 
        String channelParamList = "";
        for (String channelName : channels.split(",")) {
            channelParamList +=  ", @" + this.cache.getChannelsConfigByName(channelName).getSchemaColumn();
        }
        
        // sub user-input channels into query
        String query = Configuration.getQuery("getChannelDataSubset");
        query = query.replace("COL_LIST", channelParamList);
        
        CompactQuery compactQuery = CompactQuery.from(new CompactSchema(this.cache.getLatestSchema().getSchemaString()), "bytes", query);
        String sqlServerQuery = compactQuery.generateSqlQuery(SqlDialect.SQL_SERVER);
        
        // FIXME: ugly hack since CompactQuery doesn't seem to support JOIN and WHERE statements
        sqlServerQuery = sqlServerQuery.substring(0, sqlServerQuery.length()-1);
        sqlServerQuery += Configuration.getQuery("getChannelDataSubset2");
        
        // sub remaining user input channels into CompactQuery string
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("fromDate", LocalDate.parse(from))
                .addValue("toDate", LocalDate.parse(to));
        
        List<ChannelDataBTSubset> resultSetList = this.getNamedParameterJdbcTemplate().query(sqlServerQuery, params, new ChannelValueBTSubsetRowMapper());
        System.out.println("Result set length: " + resultSetList.size());
        
        for (ChannelDataBTSubset subsetData : resultSetList) {
            CompactData compactData = compactQuery.compactData(subsetData.getColumnData());
            ChannelDataBT channelData = new ChannelDataBT();
            channelData.setUnitId((Integer)subsetData.getColumnData().get("UnitID"));
            channelData.setUnitNumber((String)subsetData.getColumnData().get("UnitNumber"));
            channelData.setTimestamp((Timestamp)subsetData.getColumnData().get("TimeStamp"));
            channelData.setSchemaVersion(((Integer)subsetData.getColumnData().get("schema_version")));
            
            for (String channelName : channels.split(",")) {
                System.out.println("Decoding " + this.cache.getChannelsConfigByName(channelName).getSchemaColumn() + " channel from blob.");
                ChannelValueBT channelValue = new ChannelValueBT(channelName, compactData.data().get(this.cache.getChannelsConfigByName(channelName).getSchemaColumn()));
                channelData.getData().add(channelValue);
            }
            result.add(channelData);
        }
        
        return result;
    }
    
    /**
     * get data for a single unit returning only a subset of channels (uses r2m-combine CompactQuery)
     * @param from
     * @param to
     * @param channels
     * @param unitNumber
     * @return
     */
    public List<ChannelDataBT> getUnitDataSubset(String from, String to, String channels, String unitNumber) {
        List<ChannelDataBT> result = new ArrayList<ChannelDataBT>();
        
        // build input params into CompactQuery-friendly 
        String channelParamList = "";
        for (String channelName : channels.split(",")) {
            channelParamList +=  ", @" + this.cache.getChannelsConfigByName(channelName).getSchemaColumn();
        }
        
        // sub user-input channels into query
        String query = Configuration.getQuery("getChannelDataSubsetUnit");
        query = query.replace("COL_LIST", channelParamList);
        
        CompactQuery compactQuery = CompactQuery.from(new CompactSchema(this.cache.getLatestSchema().getSchemaString()), "bytes", query);
        String sqlServerQuery = compactQuery.generateSqlQuery(SqlDialect.SQL_SERVER);
        System.out.println(sqlServerQuery);
        
        // FIXME: ugly hack since CompactQuery doesn't seem to support JOIN and WHERE statements
        sqlServerQuery = sqlServerQuery.substring(0, sqlServerQuery.length()-1);
        sqlServerQuery += Configuration.getQuery("getChannelDataSubsetUnit2");
        
        // sub remaining user input channels into CompactQuery string
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("fromDate", LocalDate.parse(from))
                .addValue("toDate", LocalDate.parse(to))
                .addValue("unitNumber", unitNumber);
        
        List<ChannelDataBTSubset> resultSetList = this.getNamedParameterJdbcTemplate().query(sqlServerQuery, params, new ChannelValueBTSubsetRowMapper());

        // map from subset bean to ChannelDataBT using CompactData
        for (ChannelDataBTSubset subsetData : resultSetList) {
            CompactData compactData = compactQuery.compactData(subsetData.getColumnData());
            ChannelDataBT channelData = new ChannelDataBT();
            channelData.setUnitId((Integer)subsetData.getColumnData().get("UnitID"));
            channelData.setUnitNumber((String)subsetData.getColumnData().get("UnitNumber"));
            channelData.setTimestamp((Timestamp)subsetData.getColumnData().get("TimeStamp"));
            channelData.setSchemaVersion(((Integer)subsetData.getColumnData().get("schema_version")));
            
            for (String channelName : channels.split(",")) {
                ChannelValueBT channelValue = new ChannelValueBT(channelName, compactData.data().get(this.cache.getChannelsConfigByName(channelName).getSchemaColumn()));
                channelData.getData().add(channelValue);
            }
            result.add(channelData);
        }
        
        return result;
    }
    
    private class ChannelValueBTRowMapper implements RowMapper<ChannelDataBT> {
        public ChannelDataBT mapRow(ResultSet rs, int rowNum) throws SQLException {
            ChannelDataBT channelData = new ChannelDataBT();
            channelData.setUnitId(rs.getInt("UnitID"));
            channelData.setUnitNumber(rs.getString("UnitNumber"));
            channelData.setTimestamp(rs.getTimestamp("TimeStamp"));
            channelData.setBytes(rs.getBytes("bytes"));
            channelData.setSchemaVersion(rs.getInt("schema_version"));
            return channelData;
        }
    }
    
    private class ChannelValueBTSubsetRowMapper implements RowMapper<ChannelDataBTSubset> {
        @Override
        public ChannelDataBTSubset mapRow(ResultSet rs, int rowNum) throws SQLException {
            ChannelDataBTSubset channelData = new ChannelDataBTSubset();

            ResultSetMetaData meta = rs.getMetaData();
            
            for (int i = 1; i <= meta.getColumnCount(); i++) {
                channelData.addColumnData(meta.getColumnLabel(i), rs.getObject(meta.getColumnLabel(i)));
            }
            
            return channelData;
        }
        
    }

}
