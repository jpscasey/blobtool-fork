package com.trimble.r2m.blobtool.config;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class Configuration {
    private final static Config conf = ConfigFactory.load();

    public static Config get() {
        return conf;
    }
    
    public static String getQuery(String queryId) {
        return conf.getString("r2m.query." + queryId);
    }

}
