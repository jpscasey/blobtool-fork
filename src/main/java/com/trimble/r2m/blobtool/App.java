package com.trimble.r2m.blobtool;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.trimble.r2m.blobtool.bean.ChannelDataBT;
import com.trimble.r2m.blobtool.caching.ChannelCache;
import com.trimble.r2m.blobtool.config.Configuration;
import com.trimble.r2m.blobtool.file.FileWriterCSV;
import com.trimble.r2m.blobtool.parse.ChannelValueParser;
import com.trimble.r2m.blobtool.provider.ChannelValueProvider;
import com.trimble.r2m.blobtool.provider.DataSourceProvider;

/**
 * Blob query tool
 * @author BRedaha
 */
public class App 
{
    public static void main( String[] args )
    {
        final Configuration config = new Configuration();
        final DataSourceProvider dsProvider = new DataSourceProvider();
        final ChannelValueProvider dataProvider;
        final ChannelValueParser channelParser;
        final FileWriterCSV fileWriter = new FileWriterCSV();
        
        
        Options options = new Options();
        options.addOption(Option.builder("f").desc("fleet").required(true).hasArg(true).build());
        options.addOption(Option.builder("s").desc("start time").required(false).hasArg(true).build());
        options.addOption(Option.builder("e").desc("end time").required(false).hasArg(true).build());
        options.addOption(Option.builder("u").desc("unit number").required(false).hasArg(true).build());
        options.addOption(Option.builder("c").desc("column list").required(false).hasArg(true).build());
        
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        
        // parse command line args
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        String fleetId = null;
        if (cmd.hasOption("f")) {
            fleetId = cmd.getOptionValue("f").toUpperCase();
        }
        
        String startTime = LocalDate.now().plusDays(-1L).toString();
        if (cmd.hasOption("s") && !cmd.getOptionValue("s").equals("")) {
            startTime = cmd.getOptionValue("s");
        } else {
            System.out.println("Using default start time: " + startTime);
        }
        
        String endTime = LocalDate.now().toString();
        if (cmd.hasOption("e") && !cmd.getOptionValue("e").equals("")) {
            endTime = cmd.getOptionValue("e");
        } else {
            System.out.println("Using default end time: " + endTime);
        }
        
        String unitNumber = "all";
        if (cmd.hasOption("u") && !cmd.getOptionValue("u").equals("")) {
            unitNumber = cmd.getOptionValue("u");
        }
        
        String colList = "all";
        if (cmd.hasOption("c") && !cmd.getOptionValue("c").equals("")) {
            colList = cmd.getOptionValue("c");
        }
                
        // init cache
        ChannelCache channelCache = new ChannelCache(dsProvider.getDataSource(fleetId));
        System.out.println(channelCache.test());
        
        // get data using args
        List<ChannelDataBT> channelData = new ArrayList<ChannelDataBT>();
        
        dataProvider = new ChannelValueProvider(dsProvider.getDataSource(fleetId), channelCache);
        if (unitNumber.equals("all")) {
            channelData = dataProvider.getChannelData(startTime, endTime, colList);
            System.out.println("Queried " + String.valueOf(channelData.size()) + " channel values.");
        } else {
            channelData = dataProvider.getChannelDataUnit(startTime, endTime, colList, unitNumber);
            System.out.println("Queried " + String.valueOf(channelData.size()) + " channel values.");
        }

        // parse data out of the blob (it's is already parsed if we used a subset of columns
        if (colList.equals("all")) {
            channelParser = new ChannelValueParser(channelCache);
            channelData = channelParser.decodeDataSet(channelData);
        }
        
        System.out.println("Dataset has: " + String.valueOf(channelData.size()) + " entries");
        
        // create file from data
        try {
            fileWriter.createCSVFile(channelData);
            System.out.println("File write successful.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
}
