package com.trimble.r2m.blobtool.parse;

import java.util.ArrayList;
import java.util.List;

import com.trimble.r2m.blobtool.bean.ChannelDataBT;
import com.trimble.r2m.blobtool.bean.ChannelValueBT;
import com.trimble.r2m.blobtool.caching.ChannelCache;
import com.trimble.r2m.combine.ColumnDataMapper;
import com.trimble.r2m.combine.CompactData;
import com.trimble.r2m.combine.CompactSchema;

public class ChannelValueParser {
    
    ChannelCache cache;
    
    public ChannelValueParser(ChannelCache channelCache) {
        this.cache = channelCache;

    }
    
    public List<ChannelDataBT> decodeDataSet(List<ChannelDataBT> blobData) {
        
        CompactSchema compactSchema;
        Integer schemaId = null;
        ColumnDataMapper mapper;
        List<ChannelValueBT> channelData;
                
        for (ChannelDataBT entry : blobData) {
            
            schemaId = entry.getSchemaVersion();
            compactSchema = new CompactSchema(cache.getFleetSchema(schemaId).getSchemaString());
            
            byte[] bytes = entry.getBytes();
            
            CompactData compactData = new CompactData(compactSchema, bytes);
            mapper = compactData.data();
            
            channelData = new ArrayList<ChannelValueBT>();
            
            for (String channelName : cache.getChannelList()) {
                ChannelValueBT channelValue = new ChannelValueBT(channelName, mapper.get(this.cache.getChannelsConfigByName(channelName).getSchemaColumn()));
                channelData.add(channelValue);
            }
                        
            entry.setData(channelData);
        }
        
        return blobData;
    }

}
