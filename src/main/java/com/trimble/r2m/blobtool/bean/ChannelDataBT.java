package com.trimble.r2m.blobtool.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ChannelDataBT {
    private Integer unitId;
    private String unitNumber;
    private Timestamp timestamp;
    private byte[] bytes;
    private Integer schemaVersion;
    
    private List<ChannelValueBT> data;
    
    public ChannelDataBT() {
        this.data = new ArrayList<ChannelValueBT>();
    }
    
    public Integer getUnitId() {
        return unitId;
    }
    
    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }
    
    public String getUnitNumber() {
        return unitNumber;
    }
    
    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }
    
    public Timestamp getTimestamp() {
        return timestamp;
    }
    
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
    
    public byte[] getBytes() {
        return bytes;
    }
    
    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
    
    public Integer getSchemaVersion() {
        return schemaVersion;
    }
    
    public void setSchemaVersion(Integer schema_version) {
        this.schemaVersion = schema_version;
    }

    public List<ChannelValueBT> getData() {
        return data;
    }

    public void setData(List<ChannelValueBT> data) {
        this.data = data;
    }
}
