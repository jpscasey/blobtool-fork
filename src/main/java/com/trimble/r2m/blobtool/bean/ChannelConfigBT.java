package com.trimble.r2m.blobtool.bean;

/**
 * ChannelConfig bean, BT for blobtool until we consolidate the code in SB
 * @author BRedaha
 *
 */
public class ChannelConfigBT {
    private String name;
    private Integer id;
    private String dataType;
    private Integer minValue;
    private Double maxValue;
    private String hardwareType;
    private Integer colNum;
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public String getDataType() {
        return dataType;
    }
    
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }
    
    public Integer getMinValue() {
        return minValue;
    }
    
    public void setMinValue(Integer minValue) {
        this.minValue = minValue;
    }
    
    public Double getMaxValue() {
        return maxValue;
    }
    
    public void setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
    }
    
    public String getHardwareType() {
        return this.hardwareType;
    }
    
    public void setHardwareType(String hardwareType) {
        this.hardwareType = hardwareType;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getColNum() {
        return colNum;
    }
    public void setColNum(Integer colNum) {
        this.colNum = colNum;
    }
    
    /**
     * returns a string of the channel column name, e.g. col123
     * for use when compiling a ColumnDataMapper for blob schemas
     * @return
     */
    public String getSchemaColumn() {
        return "col" + String.valueOf(this.colNum);
    }
}