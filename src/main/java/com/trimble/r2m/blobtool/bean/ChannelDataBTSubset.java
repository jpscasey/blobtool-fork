package com.trimble.r2m.blobtool.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * ChannelData bean for a subset of channels, since r2m-combine's functionality for querying subsets requires a Map<String, Object>
 * @author BRedaha
 *
 */
public class ChannelDataBTSubset {

    private Map<String, Object> columnData;
    
    public ChannelDataBTSubset() {
        this.columnData = new HashMap<String, Object>();
    }
    
    public Map<String, Object> getColumnData() {
        return columnData;
    }
    
    public void setColumnData(Map<String, Object> columnData) {
        this.columnData = columnData;
    }
    
    public void addColumnData(String channelName, Object channelValue) {
        this.columnData.put(channelName, channelValue);
    }
    
}
