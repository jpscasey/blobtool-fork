package com.trimble.r2m.blobtool.bean;

public class ChannelValueBT {
    private String channelName;
    private Object channelValue;
    
    public ChannelValueBT(String name, Object value) {
        this.channelName = name;
        this.channelValue = value;
    }
    
    public String getChannelName() {
        return channelName;
    }
    
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
    
    public Object getChannelValue() {
        return channelValue;
    }
    
    public void setChannelValue(Object channelValue) {
        this.channelValue = channelValue;
    }

}
