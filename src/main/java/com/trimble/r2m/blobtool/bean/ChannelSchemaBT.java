package com.trimble.r2m.blobtool.bean;

public class ChannelSchemaBT {
    
    private String fleetId;
    private Integer schemaId;
    private String schemaString;
    
    public String getFleetId() {
        return fleetId;
    }
    public void setFleetId(String fleetId) {
        this.fleetId = fleetId;
    }
    
    public Integer getSchemaId() {
        return schemaId;
    }
    public void setSchemaId(Integer schemaId) {
        this.schemaId = schemaId;
    }
    
    public String getSchemaString() {
        return schemaString;
    }
    public void setSchemaString(String schemaString) {
        this.schemaString = schemaString;
    }
}
