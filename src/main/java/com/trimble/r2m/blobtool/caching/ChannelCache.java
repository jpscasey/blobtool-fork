package com.trimble.r2m.blobtool.caching;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import com.trimble.r2m.blobtool.bean.ChannelConfigBT;
import com.trimble.r2m.blobtool.bean.ChannelSchemaBT;
import com.trimble.r2m.blobtool.persistence.ChannelDao;

public class ChannelCache {

    private ChannelDao channelDao;
    
    private Map<String, ChannelConfigBT> channelMap;
    
    private Map<Integer, ChannelSchemaBT> schemaMap;
    
    public ChannelCache(DataSource ds) {
        this.channelDao = new ChannelDao(ds);

        this.channelMap = this.channelDao.getChannelConfig();
        this.schemaMap = this.channelDao.getChannelSchema();
    }
    
    /**
     * Return a channel config for a single channel
     * @param channelName
     * @return
     */
    public ChannelConfigBT getChannelsConfigByName(String channelName) {
        return channelMap.get(channelName);
    }
    
    /**
     * Get list of channels from map
     * @return
     */
    public List<String> getChannelList() {
        List<String> result = new ArrayList<String>();
        result.addAll(channelMap.keySet());
        return result;
    }
    
    /**
     * Get the r2m-combine friendly schema for decoding blob data
     * Get all schemas? Match by ID?
     * @param schemaId
     */
    public ChannelSchemaBT getFleetSchema(Integer schemaId) {
        return schemaMap.get(schemaId);
    }
    
    public ChannelSchemaBT getLatestSchema() {
        Integer maxId = 0;
        for (Integer nextInt : schemaMap.keySet()) {
            if (nextInt > maxId) {
                maxId = nextInt;
            }
        }
        
        //return schemaMap.get(schemaMap.keySet().stream().mapToInt(v -> v).max());
        return schemaMap.get(maxId);
    }
    
    public String test() {
        return "Channels mapped: " + String.valueOf(channelMap.size() + " and schemas mapped: " + String.valueOf(schemaMap.size()));
    }
    
}
